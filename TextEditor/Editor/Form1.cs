﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using YamlDotNet.Serialization;

namespace Editor
{
    public partial class TextEditor : Form
    {
        public TextEditor()
        {
            InitializeComponent();
        }

        //Open new empty text window
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
        }

        //Exit from editor
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //Open a text file
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Text files (.yaml)|*.yaml";
            ofd.Title = "Open a file...";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(ofd.FileName);
                richTextBox1.Text = sr.ReadToEnd();
                sr.Close();
            }
        }

        //Save a text file
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog svf = new SaveFileDialog();
            svf.Filter = "Text files (.yaml)|*.yaml";
            svf.Title = "Open a file...";
            if (svf.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(svf.FileName);
                sw.Write(richTextBox1.Text);
                sw.Close();
            }
        }

        //Undo an action
        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Undo();
        }

        //Redo an action
        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Redo();
        }

        //Cut text
        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Cut();
        }

        //Copy text
        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Copy();
        }

        //Paste text
        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Paste();
        }

        //Select text part
        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectAll();
        }

        //Open new empty text window
        private void newToolStripButton_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
        }

        //Open a text file
        private void openToolStripButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Text files (.yaml)|*.yaml";
            ofd.Title = "Open a file...";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(ofd.FileName);
                richTextBox2.Text = sr.ReadToEnd();
                sr.Close();
            }
        }

        //Save the text
        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog svf = new SaveFileDialog();
            svf.Filter = "Text files (.yaml)|*.yaml";
            svf.Title = "Open a file...";
            if (svf.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(svf.FileName);
                sw.Write(richTextBox2.Text);
                sw.Close();
            }
        }

        //Cut text
        private void cutToolStripButton_Click(object sender, EventArgs e)
        {
            richTextBox1.Cut();
        }

        //Copy a text
        private void copyToolStripButton_Click(object sender, EventArgs e)
        {
            richTextBox1.Copy();
        }

        //Paste a text
        private void pasteToolStripButton_Click(object sender, EventArgs e)
        {
            richTextBox1.Paste();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {

        }
        //---------------------------------------------------------------------------------------------------------------------

        private void btnBase_Click(object sender, EventArgs e) // Open file "Base"
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "YAML files (*.yaml)|*.yaml| All Files (*.*)| *.*";
            ofd.Title = "Open a file...";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(ofd.FileName);
                richTextBox1.Text = sr.ReadToEnd();
                sr.Close();
            }
        }

        private void btnComparison_Click(object sender, EventArgs e) // Open file "Comparison"
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "YAML files (*.yaml)|*.yaml| All Files (*.*)| *.*";
            ofd.Title = "Open a file...";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(ofd.FileName);
                richTextBox2.Text = sr.ReadToEnd();
                sr.Close();
            }
        }

        private void btnResults_Click(object sender, EventArgs e)
        {
            //Compare the 2 files that referenced in the texts
            if (this.richTextBox1.Text == this.richTextBox2.Text)
            {
                MessageBox.Show("files are equal");
            }
            else
            {
                MessageBox.Show("Files are not equal");
            }
        }
    }
}


